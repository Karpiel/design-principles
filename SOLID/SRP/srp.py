# SRP - Single Responsibility Principle lub SOC - Separation of Concern

class Journal:

    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f'{self.count}: {text}')

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return '\n'.join(self.entries)

# Na razie wszystko ok i nie łamiemy tutaj zasady SRP.
# Wpiszmy wiec do obiektu typu Journal kilka wpisów.

j = Journal()
j.add_entry("I cried today.")
j.add_entry("I ate a bug.")
print(f'Journal entries:\n{j}')

