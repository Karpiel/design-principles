# SRP - Single Responsibility Principle lub SOC - Separation of Concern
#
# W tym przykładzie wprowadzona zostanie koncepcja zapisywania i odczytywania
# danych z dysku. Powierzchownie wygląda to na dobry pomysł, ponieważ istotnie
# sama klasa Journal (skoro służy do przechowywania wpisów) mogłaby także
# zapisywać i odczytywać swoje dane z dysku.

class Journal:

    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f'{self.count}: {text}')

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return '\n'.join(self.entries)

    # Poniżej wprowadzam trzy metody służące do zapisu i odczytu. Stanowi to pokaz złamania zasady SRP.
    # Teraz Journal ma nową odpowiedzialność w postaci zapisywania
    # i ładowanie swoich danych. Pozornie wyglada to na dobry pomysł.
    # Zwykle zapisywanie i odczytywanie to jednak osobna odpowiedzialność.
    # Jeśli jest to realizowane przez inną część kodu (inną klasę) jeśli wpadniemy na pomysl
    # dopisana czegos do procedury save'owania, to będziemy modyfikować tylko klasę, ktora sie tym zajmuje,
    # i nie będziemy musieli zmieniać kodu w wielu innych miejscach, gdzie pojawiły się
    # mechanizmy zapisu i odczytu.

    def save_to_file(self, filename):
        print(f'Zapisuje dane')
        file = open(filename, "w")
        file.write(str(self))
        file.close()

    def load_from_file_and_print(self, filename):
        print(f'Wczytuje dane')
        with open(file) as fh:
            data = fh.read()
        print(data)
        return data

# Tutaj do strony kodu nadal wszystko jest ok i ten kod działa.
# Chodzi tutaj tylko o ujawnienie problemu niewłaściwego sposobu
# pisania programu. Poprawne rozwiązanie pokazane jest w pliku srp3.py

j = Journal()
j.add_entry("I cried today.")
j.add_entry("I ate a bug.")
print(f'Journal entries:\n{j}')

# zapisywanie
file = r'temp_journal.txt'
j.save_to_file(file)

# i zaladowanie danych
j.load_from_file_and_print(file)