# SRP - Single Responsibility Principle lub SOC - Separation of Concern
#
# W tym przykładzie metody do zapisu i odczytu danych zostały przepisane
# do osobnej klasy o nazwie PersistenceManager. Dzięki temu jedną odpowiedzialnością
# tej klasy jest właśnie zapis i odczyt danych. Klasa Journal jest niezależna od
# mechanizmu zapisu i odczytu.

class Journal:

    def __init__(self):
        self.entries = []
        self.count = 0

    def add_entry(self, text):
        self.count += 1
        self.entries.append(f'{self.count}: {text}')

    def remove_entry(self, pos):
        del self.entries[pos]

    def __str__(self):
        return '\n'.join(self.entries)

# Utworzyłem nową klasę, której odpowiedzialnością jest tylko zapisywanie
# i odczyt danych. Posiada ona statyczne metody, więc można z nich korzystać
# bezpośrednio, bez ich instancjonowania.

class PersistenceManager:

    @staticmethod
    def save_to_file(journal, filename):
        print(f'Zapisuje dane')
        file = open(filename, "w")
        file.write(str(journal))
        file.close()

    @staticmethod
    def load_from_file_and_print(filename):
        print(f'Wczytuje dane')
        with open(file) as fh:
            data = fh.read()
        print(data)
        return data


j = Journal()
j.add_entry("I cried today.")
j.add_entry("I ate a bug.")
print(f'Journal entries:\n{j}')

# Zapisywanie
file = r'temp_journal.txt'
PersistenceManager.save_to_file(j, file)

# Załadowanie danych
journal = PersistenceManager.load_from_file_and_print(file)

